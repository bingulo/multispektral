#include "WiFi.h"
#include "esp_camera.h"
#include "esp_timer.h"
#include "img_converters.h"
#include "Arduino.h"
#include "soc/soc.h"           // Disable brownour problems
#include "soc/rtc_cntl_reg.h"  // Disable brownour problems
#include "driver/rtc_io.h"
#include <ESPAsyncWebServer.h>
#include <StringArray.h>
#include <SPIFFS.h>
#include <FS.h>
#include "camera_pins.h"
#include "config_cam.h"
// Replace with your network credentials
const char* ssid = "secret";
const char* password = "secret";

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

boolean takeNewPhoto = false;

// Photo File Name to save in SPIFFS
#define FILE_IR_PHOTO "/ir-photo.jpg"
#define FILE_R_PHOTO "/r-photo.jpg"
#define FILE_G_PHOTO "/g-photo.jpg"
#define FILE_B_PHOTO "/b-photo.jpg"

#define DEBUG_LED 33
#define IR_LED 15
#define R_LED 14
#define G_LED 2
#define B_LED 13

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    body { text-align:center; }
    .vert { margin-bottom: 10%; }
    .hori{ margin-bottom: 0%; }
  </style>
</head>
<body>
  <div id="container">
    <h2>ESP32-CAM Multispektral</h2>
	  <p>Wait last led (blue) off to refresh the page.</p>
    <p>
      <button onclick="capturePhoto()">CAPTURE PHOTOS</button>
      <button onclick="location.reload();">REFRESH PAGE</button>
    </p>
	 <p>
	   <button onclick="/ir-photo">IR PHOTO</button>
	   <button onclick="/r-photo">R PHOTO</button>
	   <button onclick="/g-photo">G PHOTO</button>
	 	 <button onclick="/b-photo">B PHOTO</button>
	 </p>
</body>
<script>
  var deg = 0;
  function capturePhoto() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', "/capture", true);
    xhr.send();
  }
  function isOdd(n) { return Math.abs(n % 2) == 1; }
</script>
</html>)rawliteral";

void setup() {
  // Serial port for debugging purposes
  Serial.begin(115200);

	pinMode(DEBUG_LED, OUTPUT);
	pinMode(IR_LED, OUTPUT);
	pinMode(R_LED, OUTPUT);
	pinMode(G_LED, OUTPUT);
	pinMode(B_LED, OUTPUT);
	digitalWrite(DEBUG_LED, HIGH);
	digitalWrite(IR_LED, LOW);
	digitalWrite(R_LED, LOW);
	digitalWrite(G_LED, LOW);
	digitalWrite(B_LED, LOW);
	
  // Connect to Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }
  if (!SPIFFS.begin(true)) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    ESP.restart();
  }
  else {
    delay(500);
    Serial.println("SPIFFS mounted successfully");
  }

  // Print ESP32 Local IP Address
  Serial.print("IP Address: http://");
  Serial.println(WiFi.localIP());

  // Turn-off the 'brownout detector'
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);

  camera_config_t config = setConfig();

  // Camera init
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    ESP.restart();
  }
  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/html", index_html);
  });

  server.on("/capture", HTTP_GET, [](AsyncWebServerRequest * request) {
    takeNewPhoto = true;
    request->send_P(200, "text/plain", "Taking Photo");
  });

  server.on("/ir-photo", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, FILE_IR_PHOTO, "image/jpg", false);
  });
  server.on("/r-photo", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, FILE_R_PHOTO, "image/jpg", false);
  });
  server.on("/g-photo", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, FILE_G_PHOTO, "image/jpg", false);
  });
  server.on("/b-photo", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, FILE_B_PHOTO, "image/jpg", false);
  });

  // Start server
  server.begin();

}

void loop() {
  if (takeNewPhoto) {
    capturePhotoSaveSpiffs(IR_LED, FILE_IR_PHOTO);
    capturePhotoSaveSpiffs(R_LED, FILE_R_PHOTO);
    capturePhotoSaveSpiffs(G_LED, FILE_G_PHOTO);
    capturePhotoSaveSpiffs(B_LED, FILE_B_PHOTO);
    takeNewPhoto = false;
  }
  delay(1);
}

// Check if photo capture was successful
bool checkPhoto(fs::FS &fs, String filepath ) {
  File f_pic = fs.open(filepath);
  unsigned int pic_sz = f_pic.size();
  return ( pic_sz > 100 );
}

// Capture Photo and Save it to SPIFFS
void capturePhotoSaveSpiffs(int led, String filepath) {
  camera_fb_t * fb = NULL; // pointer
  bool ok = 0; // Boolean indicating if the picture has been taken correctly

  digitalWrite(led, HIGH);
	delay(300);
  do {
    // Take a photo with the camera
    Serial.println("Taking a photo...");

    fb = esp_camera_fb_get();
    if (!fb) {
      Serial.println("Camera capture failed");
      return;
    }

    // Photo file name
    Serial.printf("Picture file name: ");
		Serial.println(filepath);
    File file = SPIFFS.open(filepath, FILE_WRITE);

    // Insert the data in the photo file
    if (!file) {
      Serial.println("Failed to open file in writing mode");
    }
    else {
      file.write(fb->buf, fb->len); // payload (image), payload length
      Serial.print("The picture has been saved in ");
      Serial.print(filepath);
      Serial.print(" - Size: ");
      Serial.print(file.size());
      Serial.println(" bytes");
    }
    // Close the file
    file.close();
    esp_camera_fb_return(fb);

    // check if file has been correctly saved in SPIFFS
    ok = checkPhoto(SPIFFS, filepath);
  } while ( !ok );
  digitalWrite(led, LOW);
}
